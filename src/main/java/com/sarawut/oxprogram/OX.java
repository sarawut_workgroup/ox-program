/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.oxprogram;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author max-s
 */
public class OX {

    public static void main(String[] args) {
        //setting environment
        int round; //check round
        int win; //0 = no one win, 1 = O win, 2 = X win, 3 = in progress
        boolean not_err;
        boolean game_completed = false;
        Scanner in_p = new Scanner(System.in);//in_p = input_point
        char[][] ox_t = new char[3][3]; //ox_t = OX table
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                ox_t[i][j] = '-';
            }
        }

        //setting player
        //player 1 = O
        //player 2 = X
        Random turn = new Random();
        boolean player1_turn;
        boolean player2_turn;
        char current_player;

        //position input of current player
        int posx = 0;
        int posy = 0;
        //input checking int or not
        String posxC, posyC;

        //random player start
        if ((turn.nextInt(10) % 2) == 0) {
            player1_turn = true;
            player2_turn = false;
            // O start the game
        } else {
            player1_turn = false;
            player2_turn = true;
            // X start the game
        }

        //start game
        System.out.println("Welcome to OX Game");
        table_show(ox_t);
        round = 1;

        while (!game_completed && round <= 9) {//if game not complete do in loop

            //show turn player
            if (player1_turn == true) {
                System.out.println("O turn");
                current_player = 'O';
            } else {
                System.out.println("X turn");
                current_player = 'X';
            }

            int errstatus = 0;
            not_err = false;

            while (!not_err) {
                //check for 
                if (errstatus == 0){
                    System.out.println("Please input Row Col: ");
                }

                //input position
                posxC = in_p.next();
                posyC = in_p.next();
                
                //checking user input is integer or not
                if (integer_checking(posxC) && integer_checking(posyC)) {
                    posx = Integer.parseInt(posxC) - 1;
                    posy = Integer.parseInt(posyC) - 1;
                    
                    //check range of position
                    if (posx + 1 < 4 && posy + 1 < 4 && posx + 1 > 0 && posy + 1 > 0) {
                        if (position_empty(ox_t, posx, posy)) {
                            //when position is empty
                            errstatus = 0;
                        } else {
                            //when position is not empty
                            errstatus = 3;
                        }
                    } else {
                        //when user input out of table
                        errstatus = 2;
                    }
                } else {
                    //when user input mismatch
                    errstatus = 1;
                }

                //error status check
                switch (errstatus) {
                    case 0 -> {
                        //no error found
                        System.out.println("Input correct please wait");
                        ox_t[posx][posy] = current_player;
                        not_err = true;
                        break;
                    }
                    case 1 -> {
                        //input mismatch
                        System.out.println("Input mismatch! Please input again!");
                        break;
                    }
                    case 2 -> {
                        //input out of range
                        System.out.println("Your is out of table, Please input Row Col (not above 3 and less than 1)!: ");
                        break;
                    }
                    case 3 -> {
                        //position is full
                        System.out.println("Your Row Col is not empty, Please input again!: ");
                        break;
                    }
                }
            }

            //show table
            table_show(ox_t);

            //check player
            if (round > 2) {
                win = winner_checker(ox_t, round);
                switch (win) {
                    case 0 -> {
                        System.out.println("No player win...");
                        game_completed = true;
                    }
                    case 1 -> {
                        System.out.println("Player O win...");
                        game_completed = true;
                    }
                    case 2 -> {
                        System.out.println("Player X win...");
                        game_completed = true;
                    }
                }
            }

            //change turn
            player1_turn = change_turn(player1_turn);
            player2_turn = change_turn(player2_turn);

            //round update
            round++;
        }
        System.out.println("Bye bye...");

        in_p.close();
    }

    //check position in table is full or not
    private static boolean position_empty(char tb[][], int posx, int posy) {
        //check empty position
        //true = empty, false = full
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == posx && j == posy) {
                    if (tb[i][j] != '-') {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
        return true;
    }

    //check winner
    private static int winner_checker(char tb[][], int round) {
        //0 = no one win, 1 = O win, 2 = X win, 3 = in progress
        int winner = 3;

        for (int i = 0; i < 3; i++) {
            if (tb[i][0] == 'O' && tb[i][1] == 'O' && tb[i][2] == 'O') {
                winner = 1;
                break;
            } else if (tb[0][i] == 'O' && tb[1][i] == 'O' && tb[2][i] == 'O') {
                winner = 1;
                break;
            }

            if (tb[i][0] == 'X' && tb[i][1] == 'X' && tb[i][2] == 'X') {
                winner = 2;
                break;
            } else if (tb[0][i] == 'X' && tb[1][i] == 'X' && tb[2][i] == 'X') {
                winner = 2;
                break;
            }
        }

        if (tb[0][0] == 'O' && tb[1][1] == 'O' && tb[2][2] == 'O') {
            winner = 1;
        } else if (tb[2][0] == 'O' && tb[1][1] == 'O' && tb[0][2] == 'O') {
            winner = 1;
        }

        if (tb[0][0] == 'X' && tb[1][1] == 'X' && tb[2][2] == 'X') {
            winner = 2;
        } else if (tb[2][0] == 'X' && tb[1][1] == 'X' && tb[0][2] == 'X') {
            winner = 2;
        }

        if (round == 9 && winner > 2) {
            //when round = 9 and no winner found
            winner = 0;
        }

        return winner;
    }

    //show table
    private static void table_show(char tb[][]) {
        System.out.println(" 1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < 3; j++) {
                System.out.print(tb[i][j] + " ");
            }
            System.out.println("");
        }
    }

    //changing turn
    private static boolean change_turn(boolean player) {
        if (player == true) {
            return false;
        } else {
            return true;
        }
    }

    //input integer check
    private static boolean integer_checking(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception error) {
            return false;
        }
    }

}
